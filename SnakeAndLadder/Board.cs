﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;

namespace SnakeAndLadder
{
    internal class Board
    {
        public List<cell> cellList = new List<cell>();
        public Board()
        {
           //cellList = JsonConvert.DeserializeObject<List<Cell>>(File.ReadAllText(@"mockBoard.json"));
            generateBoard();
           // File.WriteAllText(@"mockBoard.json", JsonConvert.SerializeObject(cellList));
        }
        private void generateBoard()
        {
            int xPozition = 341;
            int yPozition = 512;
            bool flag = true;
            for (int index1 = 0; index1 < 10; ++index1)
            {
                for (int index2 = 0; index2 < 10; ++index2)
                {
                    this.cellList.Add(new cell()
                    {
                        number = index1 * 10 + index2 + 1,
                        x = xPozition,
                        y = yPozition,
                        height = 56,
                        width = 56,
                        color = Variables.cellColor
                    });
                    if (flag)
                        xPozition += 57;
                    else
                        xPozition -= 57;
                }
                yPozition -= 57;
                if (flag)
                    xPozition -= 57;
                else
                    xPozition += 57;
                flag = !flag;
            }
        }
    }
}