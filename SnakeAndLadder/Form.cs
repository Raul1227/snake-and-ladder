﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    public partial class Form : System.Windows.Forms.Form
    {
        gameEngine game = new gameEngine();
        public bool press = false;
        public Form()
        {
            InitializeComponent();
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        public void startButton_Click(object sender, EventArgs e)
        {
            if(!Variables.resetGame)
            {
                startButton.Text = "Roll Dice and Move";
                Variables.resetGame = true;
            }          
            this.game.start(this);
            if(gameEngine.player.winner && Variables.resetGame)
            {
                startButton.Text = "Reset Game";
            }
        }
        private void toggleButton_Click(object sender, EventArgs e)
        {
            this.press = !this.press;
            if (!this.press)
                this.pictureBox1.Show();
            else
                this.pictureBox1.Hide();
        }
    }
}
