﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Media;
using System.Threading;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    internal class gameEngine
    {
        public static Player player = new Player();
        public static Interface interfaceObj = new Interface();     
        public static Dice dice = new Dice();
        
        internal void start(Form f)
        {
            //adauga player
            PlayGame(f);
        }
        private void PlayGame(Form f)
        {
           // while(!player.winner)
            {
                playerUpdate(f);
                interfaceObj.Show(f);
                Variables.diceValueRoll = Dice.Roll();                
                f.Refresh();
                Thread.Sleep(250);
            }
        }
        private void playerUpdate(Form f)
        {
            int playerCurrentPoz = player.position;
            player.position += Variables.diceValueRoll;
            if(player.position<100)
            {
                player.piece.x = Interface.board.cellList[player.position].x + Variables.cellWidth / 4;
                player.piece.y = Interface.board.cellList[player.position].y + Variables.celllHeight / 4;
            }
            if(player.position==100)
            {
                player.winner = true;
            }
            if(player.position>100)
            {
                player.position = playerCurrentPoz;
            }
        }
    }
}