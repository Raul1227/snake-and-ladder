﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Web.UI.WebControls;

namespace SnakeAndLadder
{
    internal class Dice
    {
        public System.Windows.Forms.Panel dicePanel = new System.Windows.Forms.Panel();
        System.Windows.Forms.Label diceLabel = new System.Windows.Forms.Label();
        public System.Windows.Forms.Label diceValue = new System.Windows.Forms.Label();
        private static Random r = new Random();
        public Dice()
        {
            diceLabel.Font = new Font("Arial", 20);
            diceLabel.Text = "Dice value:";
            diceLabel.TextAlign = ContentAlignment.TopLeft;
            diceLabel.Width = 340;
            diceLabel.Height = 30;
            diceValue.Text = "0";
            diceValue.TextAlign = ContentAlignment.MiddleCenter;
            diceValue.Font = new Font("Arial", 25);
            diceValue.Width = 70;
            diceValue.Height = 70;
            diceValue.Left = 140;
            diceValue.Top = 10;
            dicePanel.Width = 340;
            dicePanel.Height = 70;
            dicePanel.Left = 0;
            dicePanel.Top = 495;
            dicePanel.BackColor = Color.Brown;
            dicePanel.Controls.Add(diceLabel);
            dicePanel.Controls.Add(diceValue);
        }
        public static int Roll()
        {
            return Dice.r.Next(1, 7);
        }
    }
}