﻿using System;
using System.Drawing;
using System.Net.Configuration;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    internal class Player
    {
        public cell piece = new cell();
        public string name;
        public int position;
        public bool winner = false;
        public Player()
        {
            name = "Default";
            position = 0;
            piece.color = Color.Blue;
            piece.x = Variables.cellStartPozX;
            piece.y = Variables.cellStartPozY;
            piece.height = this.piece.width = 25;
            piece.number = 0;
        }      
        public Player(string name, string color)
        {
            this.name = name;
            piece.color = Color.FromName(color);
            position = 0;
            piece.x = Variables.cellStartPozX;
            piece.y = Variables.cellStartPozY;
            piece.height = this.piece.width = 25;
            piece.number = position;
        }
    }
}