﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace SnakeAndLadder
{
    internal class Interface
    {
        public static Board board = new Board();
        public void Show(System.Windows.Forms.Form f)
        {
            showPlayer(f);
            showBoard(f);
            showDice(f);
            //ShowPlayerInfor(f);
        }

        private void showDice(System.Windows.Forms.Form f)
        {
            gameEngine.dice.diceValue.Text = Variables.diceValueRoll.ToString();
            f.Controls.Add(gameEngine.dice.dicePanel);
        }

        public void showBoard(System.Windows.Forms.Form f)
        {
            foreach (cell cutie in board.cellList)
            {
                cutie.show(f);
            }
        }
        public void showPlayer(System.Windows.Forms.Form f)
        {
           gameEngine.player.piece.show(f);
        }
    }
}