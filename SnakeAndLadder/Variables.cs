﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI;

namespace SnakeAndLadder
{
    internal static class Variables
    {
        internal static Color cellColor = Color.Red;
        public const int cellWidth = 56;
        public const int celllHeight = 56;
        public const int cellStartPozX = 341;
        public const int cellStartPozY = 512;
        public const int playerCellPieceSize = 25;
        public static int diceValueRoll;
        public static bool resetGame = false;
    }
}
