﻿using System.Drawing;
using System.Windows.Forms;

namespace SnakeAndLadder
{
    internal class cell
    {
        public Panel panel = new Panel();
        public Color color = new Color();
        public Label label = new Label();
        public int x;
        public int y;
        public int number;
        public int height;
        public int width;
        internal void show(System.Windows.Forms.Form f)
        {        
            panel.BackColor = this.color;
            panel.Left = this.x;
            panel.Top = this.y;
            panel.Height = this.height;
            panel.Width = this.width;      
            label.Text = this.number.ToString();
            panel.Controls.Add(label);
            f.Controls.Add(panel);
        }
    }
}